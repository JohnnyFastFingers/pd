\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{8}
\contentsline {section}{\numberline {1.1}Wprowadzenie}{8}
\contentsline {section}{\numberline {1.2}Cel i zakres pracy}{8}
\contentsline {section}{\numberline {1.3}Podzia\IeC {\l } pracy}{9}
\contentsline {chapter}{\numberline {2}Wsp\IeC {\'o}\IeC {\l }czesne systemy komunikacji}{10}
\contentsline {section}{\numberline {2.1}Rozwi\IeC {\k a}zania dotychczasowe}{10}
\contentsline {subsection}{\numberline {2.1.1}Komunikacja przewodowa}{10}
\contentsline {subsection}{\numberline {2.1.2}Komunikacja bezprzewodowa}{10}
\contentsline {subsection}{\numberline {2.1.3}Systemy automatyki budynkowej}{11}
\contentsline {section}{\numberline {2.2}Wyb\IeC {\'o}r rozwi\IeC {\k a}zania problemu}{12}
\contentsline {section}{\numberline {2.3}Standard LoRa}{13}
\contentsline {chapter}{\numberline {3}Urz\IeC {\k a}dzenia ko\IeC {\'n}cowe}{18}
\contentsline {section}{\numberline {3.1}Elementy wykorzystane w systemie opomiarowania}{18}
\contentsline {subsection}{\numberline {3.1.1}Gateway Lora - Lorank 8}{18}
\contentsline {subsection}{\numberline {3.1.2}Modu\IeC {\l } LoRa}{19}
\contentsline {subsection}{\numberline {3.1.3}Arduino Pro Mini}{19}
\contentsline {subsection}{\numberline {3.1.4}\IeC {\L }adowarka oraz zabezpieczenie ogniwa}{20}
\contentsline {subsection}{\numberline {3.1.5}Stabilizator napi\IeC {\k e}cia}{21}
\contentsline {subsection}{\numberline {3.1.6}Ogniwo}{22}
\contentsline {subsection}{\numberline {3.1.7}Czujnik temperatury i wilgotno\IeC {\'s}ci}{23}
\contentsline {subsection}{\numberline {3.1.8}Czujnik ruchu}{24}
\contentsline {subsection}{\numberline {3.1.9}Czujnik jako\IeC {\'s}ci powietrza}{25}
\contentsline {subsection}{\numberline {3.1.10}Czujnik ci\IeC {\'s}nienia}{26}
\contentsline {subsection}{\numberline {3.1.11}Tranzystor IRLZ24N}{27}
\contentsline {subsection}{\numberline {3.1.12}Rezystancyjny dzielnik napi\IeC {\k e}cia}{28}
\contentsline {section}{\numberline {3.2}Schemat po\IeC {\l }\IeC {\k a}cze\IeC {\'n} elektronicznych}{29}
\contentsline {subsection}{\numberline {3.2.1}Schemat po\IeC {\l }\IeC {\k a}cze\IeC {\'n} - program Eagle}{29}
\contentsline {subsection}{\numberline {3.2.2}P\IeC {\l }ytka drukowana}{32}
\contentsline {section}{\numberline {3.3}Programowanie, u\IeC {\.z}yte biblioteki}{34}
\contentsline {subsection}{\numberline {3.3.1}Arduino Pro Mini}{34}
\contentsline {subsection}{\numberline {3.3.2}Czujnik temperatury i wilgotno\IeC {\'s}ci}{35}
\contentsline {subsection}{\numberline {3.3.3}Czujnik ci\IeC {\'s}nienia}{35}
\contentsline {subsection}{\numberline {3.3.4}Czujnik jako\IeC {\'s}ci powietrza}{36}
\contentsline {subsection}{\numberline {3.3.5}Biblioteka LoraMAC-in-C}{36}
\contentsline {subsection}{\numberline {3.3.6}Algorytm programu}{37}
\contentsline {chapter}{\numberline {4}Schemat sieci}{38}
\contentsline {section}{\numberline {4.1}Sie\IeC {\'c} The Things Network}{38}
\contentsline {subsection}{\numberline {4.1.1}Dekodowanie danych}{38}
\contentsline {section}{\numberline {4.2}Aplikacja Internetowa}{39}
\contentsline {subsection}{\numberline {4.2.1}Komunikacja z baz\IeC {\k a} danych}{39}
\contentsline {subsection}{\numberline {4.2.2}Aplikacja HTML}{42}
\contentsline {chapter}{\numberline {5}Podsumowanie}{44}
\contentsline {chapter}{\numberline {A}Rejestracja bramki i urz\IeC {\k a}dze\IeC {\'n}}{51}
\contentsline {chapter}{\numberline {B}Opis zawarto\IeC {\'s}ci p\IeC {\l }yty CD}{53}
